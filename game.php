<?php



if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if(isset($_POST['human'])){
        $human=$_POST['human'];
    }else {
        $human="";
    }
}


if (isset($_GET["user"])) {
    $user = $_GET['user'];
} else {
    
    die("Falta el nombre del parámetro");
}



function check($computer,$human){


    if (
       $human=="piedra"  && $computer == '0' ||
       $human=="papel" && $computer == '1'  ||
       $human=="tijera" && $computer == '2'

    ) {
        return "Empate";
    } else if (

        $human=="piedra" && $computer == '2' ||
        $human=="papel"  && $computer == '0'  ||
        $human=="tijera" && $computer == '1'


    ) {
        return "Tú Ganas";
    } else if (

        $human=="piedra"  && $computer == '1' ||
        $human=="papel"  && $computer == '2'  ||
        $human=="tijera"  && $computer == '0'
    ) {

        return "Pierdes";
    }else{

        return 'Algo ha salido mal';
    }
    
    
}


function test($valor){

    $computer = rand(0, 2);

    $names =["piedra","papel","tijera"];

    switch($valor){
        case 'select':
            echo 'Selecciona una opción'; 
        break;
        case 'piedra':
            echo 'Tu jugada=' .$valor. ' Ordenador='.$names[$computer] . ' Resultado=' . check($computer,$valor);
        break;
        case 'papel':
            echo 'Tu jugada=' .$valor. ' Ordenador='.$names[$computer] . ' Resultado=' . check($computer,$valor);
        break;
        case 'tijera':
            echo 'Tu jugada=' .$valor. ' Ordenador='.$names[$computer] . ' Resultado=' . check($computer,$valor);
        break;
        case 'test':
            for ($c = 0; $c < 3; $c++) {
                for ($h = 0; $h < 3; $h++) {
        
                    $r = check($c, $names[$h]);
                    print "Humano=$names[$h] Ordenador=$names[$c] Result=$r\n";
                }
            }
        break;
    }
}



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PiedraPapelTijera</title>
</head>
<style>
    a {
        text-decoration: none;
    }
</style>

<body>
    <h2>PiedraPapelTijera</h2>
    <p><?php echo 'Bienvenido:' . $user; ?></p>
    <form method="post" action="#">
        <select name="human" id="human">
            <option value="select">Select</option>
            <option value="piedra" id="piedra">Piedra</option>
            <option value="papel" id="papel">Papel</option>
            <option value="tijera" id="tijera">Tijera</option>
            <option value="test">Test</option>
        </select>
        <input type="submit" value="Jugar">
        <button><a href="index.php">Logout</a></button>
    </form>
    <p>Selecciona una opción del menú despegable</p>
    <textarea cols="60" rows="10"><?php 
        if(isset($human)){
        test($human);
        }
            ?></textarea>

</body>

</html>
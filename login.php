<?php 

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $usuario=isset($usuario)? $_POST['user']:'';
    $password=isset($_POST['password'])? $_POST['password']:'';
    $salt= 'XyZzy12*_';
    $encriptada= hash('md5', "$salt$password");
    $stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';

    if($usuario== '' && $encriptada== ''){

        echo 'Se requiere de usuario y contraseña para acceder.';

    }else if($encriptada != $stored_hash){

        echo 'Contraseña inválida.';
    }else{

        header("Location:game.php?user=" . urldecode($_POST['user']));
        die();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login/PiedraPapelTijera</title>
</head>
<body>
    <form method="post">
    <p>Usuario:</p>
    <input type="text" name="user">
    <br>
    <p>Contraseña:</p>
    <input type="text" name="password">
    <br>
    <br>
    <input type="submit" name="login">
    </form>
    
</body>
</html>